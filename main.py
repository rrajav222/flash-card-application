from tkinter import *
import pandas
import random
BACKGROUND_COLOR = "#B1DDC6"

try:
    data = pandas.read_csv("data/words_to_learn.csv")
except FileNotFoundError:
    data = pandas.read_csv("data/french_words.csv")
finally:
    data_dict = data.to_dict(orient="records")

random_word = {}


def answerCorrect():
    try:
        data_dict.remove(random_word)
        words_to_learn = [item for item in data_dict]
        new_csv = pandas.DataFrame(words_to_learn).to_csv(
            "data/words_to_learn.csv", index=False)
        pick_random()
    except ValueError:
        canvas.itemconfig(word, text=f"Completed All Words :)",
                          font=("Arial", 15, "bold"), fill="black")


def answerWrong():
    pick_random()

# pick random word


def pick_random():
    global timer, random_word
    window.after_cancel(timer)
    try:
        random_word = random.choice(data_dict)
    except IndexError:
        canvas.itemconfig(word, text=f"Completed All Words :)",
                          font=("Arial", 15, "bold"), fill="black")
    else:
        canvas.itemconfig(card_background, image=card_front_img)
        canvas.itemconfig(title, text="French", fill="black")
        canvas.itemconfig(word, text=f"{random_word['French']}", fill="black")
        timer = window.after(3000, turn_card, random_word)


def turn_card(random_word):
    canvas.itemconfig(card_background, image=card_back_img)
    canvas.itemconfig(title, text="English", fill="white")
    canvas.itemconfig(word, text=f"{random_word['English']}", fill="white")


# window setup
window = Tk()
window.title("Flash Card")
window.config(bg=BACKGROUND_COLOR, padx=50, pady=50)
timer = window.after(3000, turn_card)

# card setup
canvas = Canvas(width=800, height=525,
                bg=BACKGROUND_COLOR, highlightthickness=0)
card_front_img = PhotoImage(file="images/card_front.png")
card_back_img = PhotoImage(file="images/card_back.png")
card_background = canvas.create_image(400, 263, image=card_front_img)

title = canvas.create_text(400, 150, text="", font=("Arial", 40, "italic"))
word = canvas.create_text(400, 263, text="", font=("Arial", 60, "bold"))
canvas.grid(column=0, row=0, columnspan=2)

# buttons
wrong_img = PhotoImage(file="images/wrong.png")
wrong_button = Button(
    image=wrong_img, highlightthickness=0, command=answerWrong)
wrong_button.grid(column=0, row=1)

right_img = PhotoImage(file="images/right.png")
right_button = Button(
    image=right_img, highlightthickness=0, command=answerCorrect)
right_button.grid(column=1, row=1)

pick_random()
window.mainloop()
